//
//  FLIROneSDKCompatTests.m
//  FLIROneSDKCompatTests
//
//  Created by Beast on 10/17/17.
//  Copyright © 2017 Rapid Application Development, LLC. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FLIROneSDKCompat.h"

//+ (id<FLIROneSDKProtocol>)sharedInstance;
//
///**---------------------------------------------------------------------------------------
// * @name Accessing the SDK Version
// *  ---------------------------------------------------------------------------------------
// */
///**
// The current version of the SDK, this will only change if you use a different version of the SDK entirely
// */
//- (NSString *) version;
//
///**
// Whether or not the default UI elements display temperatures in Celsius, or alternatively in Fahrenheit
// The default value is based on the phone's location
// */
//@property (nonatomic) BOOL userInterfaceUsesCelsius;
@interface FLIROneSDKCompatTests : XCTestCase
{
	id<FLIROneSDKProtocol> f1;
}
@end

@implementation FLIROneSDKCompatTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
	f1 = [FLIROneSDK sharedInstance];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
	f1 = NULL;
    [super tearDown];
}

- (void)test_version {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
	XCTAssertNotNil(f1.version, @"no version from f1");
}

- (void)test_userInterfaceUsesCelsius
{
	XCTAssertFalse(f1.userInterfaceUsesCelsius);
	f1.userInterfaceUsesCelsius = true;
	XCTAssertTrue(f1.userInterfaceUsesCelsius);
	f1.userInterfaceUsesCelsius = false;
	XCTAssertFalse(f1.userInterfaceUsesCelsius);
	f1.userInterfaceUsesCelsius = true;
	XCTAssertTrue(f1.userInterfaceUsesCelsius);
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
