//
//  FLIROneSDKLibraryManagerCompatTests.m
//  FLIROneSDKCompatTests
//
//  Created by Beast on 10/17/17.
//  Copyright © 2017 Rapid Application Development, LLC. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FLIROneSDKLibraryManagerCompat.h"



// + (id<FLIROneSDKLibraryManagerProtocol>)sharedInstance;
//
// /**---------------------------------------------------------------------------------------
// * @name Other Methods
// *  ---------------------------------------------------------------------------------------
// */
///**
// Retrieves a thumbnail for a previously captured video or photo.
//
// @param filepath The filepath to retrieve the thumbnail for
//
// @return A UIImage representation of the file's thumbnail, or nil if the file does not exist.
//
// @see filepaths
// @see [FLIROneSDKStreamManager capturePhotoWithFilepath:]
// */
//- (nullable UIImage *) thumbnailForFilepath:(NSURL *)filepath;
//
///**
// Retrieves a preview image for a previously captured video.
//
// @param filepath The video filepath to retrieve the preview for
//
// @return A UIImage representation of the video's preview image, or nil if the video does not exist.
//
// @see filepaths
// @see [FLIROneSDKStreamManager startRecordingVideoWithFilepath:withVideoRendererDelegate:]
// */
//- (nullable UIImage *) previewImageForVideoWithFilepath:(NSURL *)filepath;
//
///**
// Returns a new filepath in the media folder with a given extension based on the current date/time
//
// @param extension The file type extension to use, e.g. "png"
// @return A filepath to a file in the media folder with the given extension
// */
//- (nullable NSURL *) libraryFilepathForCurrentTimestampWithExtension:(NSString *)extension;
//
///**
// Deletes an image or video file from the library.
//
// @param filepath The filepath of the file to delete
//
// @return YES if the file was found and deleted, otherwise NO.
//
// @see filepaths
// @see [FLIROneSDKStreamManager capturePhotoWithFilepath:]
// */
//- (BOOL) deleteLibraryFileWithFilepath:(NSURL *)filepath;
//
///**
// Copies an image or video into the library.
//
// @param filepath The filepath of the file to copy into the library directory
//
// @param thumbnailFilepath The thumbnail filepath for the thumbnail of the image
//
// @return YES if the file was successfully copied, otherwise NO.
//
// @warning If a file with the same filename exists in the library folder, the copy operation will fail
// */
//- (BOOL) copyImageToLibraryWithFilepath:(NSURL *)filepath withThumbnailFilepath:(NSURL *)thumbnailFilepath;
//
///**
// @param filepath The filepath of the file to check
//
// @return YES if the file with the specified name exists and is a video, otherwise NO.
//
// @see thumbnailForFilepath:
// */
//- (BOOL)fileIsVideo:(NSURL *)filepath;
///**---------------------------------------------------------------------------------------
// * @name Properties
// *  ---------------------------------------------------------------------------------------
// */
///**
// An array of filepaths for all images and videos in the library.
// Each item is an NSURL representing the location of the image or video.
//
// @see thumbnailForFilepath:
// @see deleteLibraryFileWithFilepath:
// */
//@property (readonly, nonatomic, strong) NSArray *filepaths;
//
//
///**
// The string representation of the folder on the device where the library media is stored.
//
// Any media saved to this folder should appear in the library UI. In order to add media to this folder,
// the FLIROneSDKImageEditor's finishEditingAndSaveCurrentImageToFilepath:withPreviewImage: method should be used.
// */
//@property (readonly, nonatomic, strong) NSURL *mediaPath;


@interface FLIROneSDKLibraryManagerCompatTests : XCTestCase
{
	id<FLIROneSDKLibraryManagerProtocol> libMan;
	NSBundle *testBundle;
}
@end

@implementation FLIROneSDKLibraryManagerCompatTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
	libMan = [FLIROneSDKLibraryManager sharedInstance];
	testBundle = [NSBundle bundleForClass:[self class]];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
	testBundle = nil;
	libMan = nil;
    [super tearDown];
}

- (void)test_thumbnailForFilepath {
	NSURL *video_url = [testBundle URLForResource:@"flir1" withExtension:@"jpg"];
	NSURL *libURL = [libMan.mediaPath URLByAppendingPathComponent:@"flir1.jpg"];

	XCTAssertNotNil(video_url,@"missing asset in bundle");
	[libMan copyImageToLibraryWithFilepath:video_url withThumbnailFilepath:NULL];
	XCTAssertNotNil([libMan thumbnailForFilepath:libURL]);
}

- (void)test_previewImageForVideoWithFilepath {
	NSURL *video_url = [testBundle URLForResource:@"IMG_1508" withExtension:@"mov"];
	NSURL *libURL = [libMan.mediaPath URLByAppendingPathComponent:@"IMG_1508.mov"];
	XCTAssertNotNil(video_url,@"missing asset in bundle");
	[libMan copyImageToLibraryWithFilepath:video_url withThumbnailFilepath:NULL];
	XCTAssertNotNil([libMan previewImageForVideoWithFilepath:libURL]);
}

- (void)test_libraryFilepathForCurrentTimestampWithExtension {
	NSString *const path_extension = @"png";
	NSURL *path = [libMan libraryFilepathForCurrentTimestampWithExtension:path_extension];
	XCTAssertNotNil(path);
	XCTAssertEqualObjects([path pathExtension], path_extension, @" ext %@ != %@!!!", [path pathExtension], path_extension);
	XCTAssertTrue([[path path] hasPrefix:[libMan mediaPath].path]);
}

- (void)test_deleteLibraryFileWithFilepath {
	NSURL *video_url = [testBundle URLForResource:@"flir1" withExtension:@"jpg"];
	XCTAssertNotNil(video_url,@"missing asset in bundle");
	[libMan copyImageToLibraryWithFilepath:video_url withThumbnailFilepath:NULL];
	XCTAssertTrue([libMan deleteLibraryFileWithFilepath:[NSURL URLWithString:@"flir1.jpg"]], @"could not delete test file: %@",libMan.filepaths);
}

- (void)test_deleteLibraryFileWithFilepath_using_full_path {
	NSURL *video_url = [testBundle URLForResource:@"flir1" withExtension:@"jpg"];
	XCTAssertNotNil(video_url,@"missing asset in bundle");
	[libMan copyImageToLibraryWithFilepath:video_url withThumbnailFilepath:NULL];
	NSLog(@"contents of mediapath %@",libMan.filepaths);
	XCTAssertTrue([libMan deleteLibraryFileWithFilepath:[libMan.mediaPath URLByAppendingPathComponent:@"flir1.jpg"]], @"could not delete test file: %@",libMan.filepaths);
}

- (void)test_deleteLibraryFileWithFilepath_when_not_in_library {
	XCTAssertFalse([libMan deleteLibraryFileWithFilepath:[libMan.mediaPath URLByAppendingPathComponent:@"non-existant-flir1.jpg"]], @"claimed to  delete non-existant file");
}


- (void)test_copyImageToLibraryWithFilepath {
	// TODO
}

- (void)test_fileIsVideo_with_real_video {
	NSURL *video_url = [testBundle URLForResource:@"IMG_1508" withExtension:@"mov"];
	XCTAssertNotNil(video_url,@"missing asset in bundle");
	[libMan copyImageToLibraryWithFilepath:video_url withThumbnailFilepath:NULL];
	XCTAssertTrue([libMan fileIsVideo:[video_url lastPathComponent]]);
}

- (void)test_fileIsVideo_with_photo {
	NSURL *video_url = [testBundle URLForResource:@"flir1" withExtension:@"jpg"];
	XCTAssertNotNil(video_url,@"missing asset in bundle");
	[libMan copyImageToLibraryWithFilepath:video_url withThumbnailFilepath:NULL];
	XCTAssertFalse([libMan fileIsVideo:[video_url lastPathComponent]]);
}

- (void)test_filepaths {
	NSArray *paths = [libMan filepaths];
	XCTAssertNotNil(paths, @"failed to return valid paths");
}

- (void)test_mediaPath {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
	NSURL *mediaURL = [libMan mediaPath];
	NSLog(@"mediaurl = %@",mediaURL);
	XCTAssertNotNil(mediaURL, @"shared instance did not return a valid url");
	
}



//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
