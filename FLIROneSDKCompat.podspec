#
#  Be sure to run `pod spec lint ObjectDetection.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "FLIROneSDKCompat"
  s.version      = "0.0.1.LOCAL"
  s.summary      = "FLIROneSDK Compatibility"

  s.description  = <<-DESC
                   FLIROneSDKCompat is a backwards compatible version of the latest (Gen3 enabled) FLIROneSDK.

                   DESC

  s.homepage     = "https://bitbucket.org/RapidApplicationDevelopment/ThermalLocations.git"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  # s.license      = "MIT (example)"
  s.license      = { :type => "MIT", :file => "LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.author             = { "Clayton Thomas" => "clay@rapidappdevel.com" }

  s.platform     = :ios, "7.0"

  s.source       = { :git => "https://RapidApplicationDevelopment@bitbucket.org/RapidApplicationDevelopment/FLIROneSDKCompat.git", :tag => s.version.to_s }

  s.source_files  = "FLIROneSDKCompat/compat/**/*.{h,hpp,m,mm,cpp,c}"
  #s.private_header_files = "ThermalLocations/Locations/details/private/**/*.{h,hpp,m,mm,cpp,c}", "ThermalLocations/Locations/details/ThermLocEx2_PrivateExtension.h", "ThermalLocations/Locations/details/RADFLIROne_PrivateExtension.h"
  s.public_header_files = "FLIROneSDKCompat/compat/**/*.{h,hpp}"
  s.header_dir = 'FLIROneSDKCompat'
  s.header_mappings_dir = 'FLIROneSDKCompat/compat'

  s.requires_arc = true

  s.dependency 'flironesdk-latest'

end
