//
//  FLIROneSDKCompat.h
//  FLIROneSDKCompat
//
//  Created by Beast on 10/18/17.
//  Copyright © 2017 Rapid Application Development, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FLIROneSDK/FLIROneSDK.h>

@protocol FLIROneSDKProtocol
+ (id<FLIROneSDKProtocol>)sharedInstance;

/**---------------------------------------------------------------------------------------
 * @name Accessing the SDK Version
 *  ---------------------------------------------------------------------------------------
 */
/**
 The current version of the SDK, this will only change if you use a different version of the SDK entirely
 */
- (NSString *) version;

/**
 Whether or not the default UI elements display temperatures in Celsius, or alternatively in Fahrenheit
 The default value is based on the phone's location
 */
@property (nonatomic) BOOL userInterfaceUsesCelsius;
@end

@interface FLIROneSDK (FLIROneSDKProtocol) <FLIROneSDKProtocol>

@end
