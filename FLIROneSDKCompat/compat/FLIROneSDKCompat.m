//
//  FLIROneSDKCompat.m
//  FLIROneSDKCompat
//
//  Created by Beast on 10/18/17.
//  Copyright © 2017 Rapid Application Development, LLC. All rights reserved.
//

#import "FLIROneSDKCompat.h"
#import <objc/runtime.h>

@implementation FLIROneSDK (FLIROneSDKProtocol)


/**---------------------------------------------------------------------------------------
 * @name Accessing the SDK Version
 *  ---------------------------------------------------------------------------------------
 */
/**
 The current version of the SDK, this will only change if you use a different version of the SDK entirely
 */
- (NSString *) version
{
	return [self.class version];
}

/**
 Whether or not the default UI elements display temperatures in Celsius, or alternatively in Fahrenheit
 The default value is based on the phone's location
 */
- (void)setUserInterfaceUsesCelsius:(BOOL)userInterfaceUsesCelsius
{
	objc_setAssociatedObject(self, @selector(userInterfaceUsesCelsius), [NSNumber.alloc initWithBool:userInterfaceUsesCelsius], OBJC_ASSOCIATION_RETAIN);
}
- (BOOL)userInterfaceUsesCelsius
{
	return [(NSNumber *)objc_getAssociatedObject(self, _cmd) boolValue];
}
@end
