//
//  FLIROneSDKLibraryManagerCompat.m
//  FLIROneSDKCompat
//
//  Created by Beast on 10/17/17.
//  Copyright © 2017 Rapid Application Development, LLC. All rights reserved.
//

#import "FLIROneSDKLibraryManagerCompat.h"
#import <FLIROneSDK/FLIROneSDKLibraryManager.h>
#import <MobileCoreServices/MobileCoreServices.h> // for mime type checks
#import <AVFoundation/AVFoundation.h> // for video preview image
#import "FLIROneSDKCompatDefs.h"

#if FLIRONESDK_LIBRARY_VERSION_IS_SIMPLE_G3
#define FLIRONESDK_LIBRARY_RECEIVER FLIROneSDKLibraryManager
#else
#define FLIRONESDK_LIBRARY_RECEIVER [FLIROneSDKLibraryManager sharedInstance]



#endif

#define IMAGE_THUMBNAIL_EXT __RAD_F1_IMAGE_THUMBNAIL_EXT
#define VIDEO_THUMBNAIL_EXT __RAD_F1_VIDEO_THUMBNAIL_EXT
static __attribute__ ((__visibility__("hidden"))) NSString * const __RAD_F1_IMAGE_THUMBNAIL_EXT = @"thumb";
static __attribute__ ((__visibility__("hidden"))) NSString * const __RAD_F1_VIDEO_THUMBNAIL_EXT = @"preview";



#if FLIRONE_SDK_COMPAT_USE_INTERNAL_METHODS
#import <FLIROneSDK/FLIROneSDK.h>

@interface FLIROneSDK (INTERNAL_EXTENSIONS_RAD)
+ (instancetype)sharedInstance;
- (void)dispatchSafely:(dispatch_block_t)block;
- (void)dispatchOnMainQueueSafely:(dispatch_block_t)block;
@end
#endif

#define dispatch_safely(block) _rad_do_dispatch_safely((block))
static __attribute__((visibility("hidden")))
void  _rad_do_dispatch_safely(dispatch_block_t block) {
#if FLIRONE_SDK_COMPAT_USE_INTERNAL_METHODS
	[[FLIROneSDK sharedInstance] dispatchSafely:block];
#else
	
	static dispatch_queue_t sync_q;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sync_q = dispatch_queue_create("com.rad.FLIROneSDKEventQueue", 0x0);
		id i = [FLIROneSDKLibraryManager sharedInstance];
		dispatch_queue_set_specific(sync_q, (__bridge const void * _Nonnull)(i), (__bridge void * _Nullable)(i), NULL);
	});
	id i = [FLIROneSDKLibraryManager sharedInstance];
	id cur_spec = (__bridge id)(dispatch_get_specific((__bridge const void * _Nonnull)(i)));
	id saved_spec = dispatch_queue_get_specific(sync_q, (__bridge const void * _Nonnull)(i)));
	if (cur_spec == saved_spec) {
		block();
	} else {
		dispatch_sync(sync_q, block);
	}
#endif
}


@implementation UIImage (rad_ext_ThumbnailAdditions)

static __attribute__ ((__visibility__("hidden"), __always_inline__))
CGFloat rad_ext_cgmax(CGFloat value1, CGFloat value2) {
	return value1 < value2 ? value2 : value1;
}
- (UIImage *)rad_ext_thumbnailOfSize:(CGSize)size {
	CGSize selfsize = self.size;
	CGFloat scale = rad_ext_cgmax(size.width/selfsize.width, size.height/selfsize.height);
	CGFloat width = selfsize.width * scale;
	CGFloat height = selfsize.height * scale;
	CGRect imageRect = CGRectMake((size.width - width)/((CGFloat)(2.0)),
								  (size.height - height)/((CGFloat)(2.0)),
								  width,
								  height);
	
	UIGraphicsBeginImageContextWithOptions(size, NO, 0);
	[self drawInRect:imageRect];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

- (UIImage *)rad_ext_hkThumbnail {
	return [self rad_ext_thumbnailOfSize:CGSizeMake(78, 78)];
}
@end

@implementation FLIROneSDKLibraryManager (FLIROneSDKLibraryManagerCompat)
#if FLIRONESDK_LIBRARY_VERSION_IS_SIMPLE_G3

- (instancetype)init
{
	return nil;
}

- (instancetype)initProtected
{
	self = [super init];
	if (self) {
		
	}
	return self;
}

+ (id<FLIROneSDKLibraryManagerProtocol>)sharedInstance
{
	static id<FLIROneSDKLibraryManagerProtocol> shared_instance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		shared_instance = [[self alloc] initProtected];
	});
	return shared_instance;
}



- (nullable NSURL *) libraryFilepathForCurrentTimestampWithExtension:(NSString *)extension {
	return [FLIRONESDK_LIBRARY_RECEIVER libraryFilepathForCurrentTimestampWithExtension:extension];
}

- (NSURL *)mediaPath
{
	return [NSURL fileURLWithPath:[FLIRONESDK_LIBRARY_RECEIVER mediaPath].path];
}










#warning Do we know if this normally returns true if filepath DNE???
- (BOOL) deleteLibraryFileWithFilepath:(NSURL *)filepath
{
//	dispatch_safely(^{
////		NSURL *path =
//		NSURL *thumbnail = [self thumbnailPathForFilepath:filepath];
//
//
//	});
	
	NSURL *itemURL = [self.class lm_media_url_for_file_url:filepath];
	NSFileManager *man = [[NSFileManager alloc] init];
	NSError *err = nil;
	BOOL result = [man removeItemAtURL:itemURL error:&err];
	if (result) {
		NSURL *thumbURL = [itemURL URLByAppendingPathExtension:IMAGE_THUMBNAIL_EXT];
		if ([man fileExistsAtPath:[thumbURL path]]) {
			result = [man removeItemAtURL:thumbURL error:&err];
		}
		thumbURL = [itemURL URLByAppendingPathExtension:VIDEO_THUMBNAIL_EXT];
		if ([man fileExistsAtPath:[thumbURL path]]) {
			result = [man removeItemAtURL:thumbURL error:&err];
		}
	}
	return result;
}

- (BOOL) copyImageToLibraryWithFilepath:(NSURL *)filepath withThumbnailFilepath:(NSURL *)thumbnailFilepath
{
	NSFileManager *man = [[NSFileManager alloc] init];
	NSURL *mediapath = [self mediaPath];
	NSURL *fileURL = NULL;
	NSURL *thumbURL = NULL;
	NSError *err = NULL;
	BOOL result = NO;
	
	if (filepath && filepath.isFileURL) {
		fileURL = [mediapath URLByAppendingPathComponent:[filepath lastPathComponent]];
		result = [man copyItemAtURL:filepath toURL:fileURL error:&err];
	}
	if (result && thumbnailFilepath && thumbnailFilepath.isFileURL) {
#warning We should probably rename the thumbnail path to end in .thumb and have the filepath last path component as its name
		thumbURL = [mediapath URLByAppendingPathComponent:[thumbnailFilepath lastPathComponent]];
		result = [man copyItemAtURL:thumbnailFilepath toURL:thumbURL error:&err];
	}
	
	
	return result;
	

}

- (BOOL)fileIsVideo:(NSURL *)filepath {
	NSString *mime = [self getMIMETypeForFilename:filepath defaultMIMEType:@"image/JPEG"];
	return [[[mime componentsSeparatedByString:@"/"] firstObject] isEqualToString:@"video"];
}

#warning CHECK THAT THIS STILL WORKS EVEN IF WE GIVE IT AN IMAGE PATH
- (nullable UIImage *)previewImageForVideoWithFilepath:(NSURL *)filepath {
	@autoreleasepool {
		NSURL *previewURL = [self lm_previewImageFilepathURLForVideoWithFilename:filepath];
		if (previewURL) {
			UIImage *result = [UIImage imageWithContentsOfFile:previewURL.absoluteString];
			if (result == NULL) {
				
				result = [self lm_generatePreviewImageForVideoWithFilename:filepath];
				NSData *jpegdata = UIImageJPEGRepresentation(result, 0.6);
				if (jpegdata) {
					[jpegdata writeToURL:previewURL atomically:YES];
				}
			}
			return result;
		}
		return NULL;
	}
}


- (nullable UIImage *)thumbnailForFilepath:(NSURL *)filepath {
	@autoreleasepool {
		NSFileManager *man = [NSFileManager defaultManager];
		filepath = [self.class lm_media_url_for_file_url:filepath];
		NSURL *thumbURL = [self.class lm_check_and_return_thumb_url_for_media_url:filepath];
		UIImage *img = nil;
		if (YES == [man fileExistsAtPath:thumbURL.path]) {
			img = [[UIImage alloc] initWithContentsOfFile:thumbURL.path];
		} else {
			if (YES == [man fileExistsAtPath:filepath.path]) {
				
				img = [[UIImage alloc] initWithContentsOfFile:filepath.path];
				if (NULL != img) {
					img = [img rad_ext_hkThumbnail];
					if (NULL != img) {
						[UIImageJPEGRepresentation(img, .5) writeToURL:thumbURL atomically:YES];
					}
				}
			}
		}
		return img;
	}
}


- (NSArray *)filepaths
{
	NSString *mediapath_path = [self mediaPath].path;
	NSArray *arr = [[NSFileManager defaultManager] subpathsAtPath:mediapath_path];
	NSMutableArray *filepaths = [[NSMutableArray alloc] initWithCapacity:[arr count]];
	for (NSString *f in arr) {
		if ([f.pathExtension isEqualToString:IMAGE_THUMBNAIL_EXT] || [f.pathExtension isEqualToString:VIDEO_THUMBNAIL_EXT]) {
			continue;
		}
		
		[filepaths addObject:[mediapath_path stringByAppendingPathComponent:f]];
	}
	return [filepaths copy];
}

#pragma mark - Internal -

- (UIImage *)lm_generatePreviewImageForVideoWithFilename:(NSURL *)arg2
{
	NSURL *r0 = [self.class lm_media_url_for_file_url:arg2];
	AVAsset *r10 = [AVAsset assetWithURL:r0];
	AVAssetImageGenerator *r8 = [[AVAssetImageGenerator alloc] initWithAsset:r10];//[_objc_msgSend(@class(AVAssetImageGenerator), *(("userInfo" | 0x0) + 0xdea)) initWithAsset:r10];
//	_CMTimeMake((sp - 0x54) + 0x1c, 0x1, 0x0, 0x1);
	CMTime t = CMTimeMakeWithSeconds(1.0, 1);
	CGImageRef r4 = [r8 copyCGImageAtTime:t actualTime:NULL error:NULL];
	UIImage *r5 = [UIImage imageWithCGImage:r4];
	CGImageRelease(r4);
	return r5;
}

- (nullable NSURL  *)lm_previewImageFilepathURLForVideoWithFilename:(NSURL *)filepath
{
	if ([self fileIsVideo:filepath]) {
		NSURL *medpath = [self.class lm_media_url_for_file_url:filepath];
	
		return [medpath URLByAppendingPathExtension:VIDEO_THUMBNAIL_EXT];
	}
	return NULL;
}

+ (nullable NSURL *)lm_ensure_path_is_atleast_inside_mediapath:(NSURL *)name
{
	if ([[name absoluteString] hasPrefix:[self mediaPath].absoluteString]) {
		return name;
	}
	return [[self mediaPath] URLByAppendingPathComponent:[name lastPathComponent]];
}

+ (nullable NSURL *)lm_media_url_for_file_url:(NSURL *)name
{
	return [NSURL fileURLWithPath:[self lm_ensure_path_is_atleast_inside_mediapath:name].path];
}

+ (NSURL *)lm_thumb_url_for_media_url:(NSURL *)url
{
	return [url URLByAppendingPathExtension:IMAGE_THUMBNAIL_EXT];
}

+ (NSURL *)lm_check_and_return_thumb_url_for_media_url:(NSURL *)url
{
	if ([[url pathExtension] isEqualToString:IMAGE_THUMBNAIL_EXT]) {
		return [NSURL fileURLWithPath:url.path];
	}
	return [self lm_thumb_url_for_media_url:url];
}

- (NSURL *)thumbnailPathForFilepath:(NSString *)arg2
{
	return [self thumbnailPathForFilename:[arg2 lastPathComponent]];
}

- (NSURL *)thumbnailPathForFilename:(NSString *)arg2
{
	NSString *r6 = [[[self filePathURLForFileWithFilename:arg2] path] stringByAppendingPathExtension:IMAGE_THUMBNAIL_EXT];

	return [NSURL fileURLWithPath:r6];
}

- (NSURL *)filePathURLForFileWithFilename:(NSString *)arg2
{
	return [[[self mediaPath] URLByAppendingPathComponent:arg2] filePathURL];
}

- (NSString *)getMIMETypeForFilename:(NSURL *)filename defaultMIMEType:(NSString *)def
{
	CFStringRef extr6 = UTTypeCreatePreferredIdentifierForTag(/* **0x72e4 */ kUTTagClassFilenameExtension, (__bridge CFStringRef)([filename pathExtension]), NULL);
	NSString *result = def;
	if (extr6 != NULL) {
		CFStringRef extr4 = UTTypeCopyPreferredTagWithClass(extr6, /* **0x72e8 */ kUTTagClassMIMEType);
		
		if (extr4 != NULL) {
			result = CFBridgingRelease(extr4);
		}
		CFRelease(extr6);
	}
	return result;
}
		   
#endif
@end




















