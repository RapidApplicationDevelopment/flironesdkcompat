//
//  main.m
//  FLIROneSDKCompat
//
//  Created by Beast on 10/17/17.
//  Copyright © 2017 Rapid Application Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
