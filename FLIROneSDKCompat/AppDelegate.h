//
//  AppDelegate.h
//  FLIROneSDKCompat
//
//  Created by Beast on 10/17/17.
//  Copyright © 2017 Rapid Application Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

