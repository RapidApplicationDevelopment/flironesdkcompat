# FLIROneSDK backwards compatibility library for version 0.4.11+ to be more like 0.4.10-
---

The iOS version 0.4.11 of the FLIROneSDK made breaking changes to its public interface. This library is a drop-in fix for backwards compatibility.

#### Relevant code is found inside `FLIROneSDKCompat/compat` (cocoapods will handle this for you).

See `Podfile` for pulling this pod into your project. The `Podfile` assumes a development build, in which the environment variable `POD_LOCAL_HOME` must be defined. 

To use in release, simply source `https://RapidApplicationDevelopment@bitbucket.org/RapidApplicationDevelopment/aathermpodrepo.git` before cocoapod's main repo and pull `pod 'flironesdk-latest'` from there, rather than pulling from a locally mounted directory.
